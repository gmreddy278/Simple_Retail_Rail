package com.retail.core;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class Itemtest {



	private Item item;

	@Before
	public void setUp() {
		item = new Item(new BigInteger("567321101987"), "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58,
				ShipMethod.AIR);
	}

	@Test
	public void testUpcLength() {
		int expected = 12;
		int actual = item.getUpc().toString().length();
		System.out.println("Junit for upc length test");
		assertEquals(expected, actual);
		
	}

	@Rule
	public ExpectedException anExce = ExpectedException.none();

	@Test
	public void testItemNegativeWeight() {
		anExce.expect(IllegalArgumentException.class);
		System.out.println(item.getWeight());
		double actual = -item.getWeight();
		System.out.println("Junit for item negative weight value");
		if (actual < 0)
			throw new IllegalArgumentException();

	}
	
	@Test
	public void testItemPostiveWeight() {
		double expected = 0.58;
		double actual = item.getWeight();
		System.out.println("Junit for item positive weight value");
		assertEquals(expected, actual,0.001);

	}
	
	@Test
	public void testItemNegativePrice() {
		anExce.expect(IllegalArgumentException.class);
		System.out.println(item.getPrice());
		double actual = -item.getPrice();
		System.out.println("Junit for item negative Price value");
		if (actual < 0)
			throw new IllegalArgumentException();

	}
	
	@Test
	public void testItemPositivePrice() {
		double expected = 19.99;
		double actual = item.getPrice();
		System.out.println("Junit for item positive Price value");
		assertEquals(expected, actual,0.001);

	}
	
	@Test
	public void testShippingMethod() {
		String actual = item.getShipMethod().toString();
		String expected = "AIR";
		System.out.println(item.getShipMethod());
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testRailMethod() {
		String actual = item.getShipMethod().toString();
		String expected = "RAIL";
		System.out.println(item.getShipMethod());
		assertEquals(expected, actual);
		
	}

}
