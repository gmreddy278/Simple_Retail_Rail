package com.retail.core;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class ItemClient {
	
	final static Logger logger = Logger.getLogger(ItemClient.class);

	public static void main(String[] args) {
		List<Item> items = new ArrayList<>();
			items.add(new Item(new BigInteger("567321101987"), "CD � Pink Floyd, Dark Side Of The Moon", 19.99, 0.58,
				ShipMethod.AIR));
		items.add(new Item(new BigInteger("567321101986"), "CD � Beatles, Abbey Road", 17.99, 0.61, ShipMethod.GROUND));
		items.add(new Item(new BigInteger("567321101985"), "CD � Queen, A Night at the Opera", 20.49, 0.55,
				ShipMethod.AIR));
		items.add(new Item(new BigInteger("567321101984"), "CD � Michael Jackson, Thriller", 23.88, 0.50,
				ShipMethod.GROUND));
		items.add(new Item(new BigInteger("467321101899"), "iPhone - Waterproof Case", 9.75, 0.73, ShipMethod.AIR));
		items.add(new Item(new BigInteger("477321101878"), "iPhone -  Headphones", 17.25, 3.21, ShipMethod.GROUND));

		items.sort(new ItemComparator());
	
		System.out.print("****SHIPMENT REPORT****					Date:" + LocalDateTime.now());
		System.out.println();
		System.out.println();
		// header

		System.out.printf("%-15s %-40s %-10s %-10s %-15s %-10s", "UPC", "Description", "Price", "Weight", "Ship Method",
				"Shipping Cost");
		System.out.println();
		// values
		for (Item item : items) {
			System.out.printf("%-15s %-40s %-10.2f %-10.2f %-15s %-10.2f", item.getUpc(), item.getDescription(),
					item.getPrice(), item.getWeight(), item.getShipMethod(), item.getShippingCost());
			System.out.println();
		}
		System.out.println();
		// total cost
		 System.out.printf("%s %79.2f", "TOTAL SHIPPING COST:",Item.totalShippingCost(items));
		 System.out.println();
		
	}
	
	
	
	

	
}
