package com.retail.core;

public interface ShippingMethod {
	
	public double getItemShippingCost(Item item);
	
	
}
