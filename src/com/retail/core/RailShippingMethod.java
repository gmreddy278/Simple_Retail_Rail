package com.retail.core;

public class RailShippingMethod implements ShippingMethod {

	@Override
	public double getItemShippingCost(Item item) {
		if (item.getWeight() < 5)
			return 5.0;
		else
			return 10.0;
	}

}
